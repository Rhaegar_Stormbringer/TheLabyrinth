![Main Menu of the game](Art/banner.png "Main Menu of the game")

# Introduction

**The Labyrinth** is a reproduction of the [Ball-in-a-Maze Puzzle](https://en.wikipedia.org/wiki/Ball-in-a-maze_puzzle) game using the [Unity Game Engine](https://unity.com/). In these dexterity board games, the goal is to manipulate one or more balls in a maze until they reach a certain goal, in general
reach a hole at the end of the labyrinth. As a classic game, many variations of this game see the light over the years, [Perplexus](https://en.wikipedia.org/wiki/Perplexus) and the [Money Maze](https://www.amazon.fr/money-maze/s?k=money+maze) being two of the most recent and popular examples. As for our project, it is directly inspired by another version : the wooden labyrinth is tilted using two knobs and the ball has to be navigated past a series of holes and obstacles.

## Why doing this project

The main idea behind this project was to put our skills with Unity into practice in a "real" project. Indeed, as we both played this particular variation of the wooden game, and [both](https://gitlab.com/TheSwaglordDracool/escape-game-unity-vr) spent some times [getting used](https://gitlab.com/Rhaegar_Stormbringer/ShmupOne) to the Unity Engine, it appears to us that it was the perfect occasion to pool skills and cover, we believe, a large portion of the areas of the development of videogames, from technical areas such as creating 3D models and assets or scripting, to more human constraints like working as a team and scheduling and documenting our work.

## Licensing

**The Labyrinth** is a little game and a reproduction of a classic board game made during our free time to perfect our understanding of Unity, and so without any kind of ambition of commercialization. Thus, it seems fair to us that the provided source code, images, sprites, graphical assets and 3D models, are licensed under the [MIT License](http://opensource.org/licenses/MIT)

The MIT License (MIT)

Copyright (c) 2020 [The Swaglord Dracool](https://gitlab.com/TheSwaglordDracool) & [Rhaegar Stormbringer](https://gitlab.com/Rhaegar_Stormbringer)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# The Game

## Welcome to the Maze !

In **The Labyrinth**, the player is represented by a shiny green ball trapped in an artificial Maze that it can tilt as it pleases. Using this ability, it must move the ball through the maze in order to reach the exit, represented by a green hole, while avoiding the deadly red holes it will encounter in its path. **The Labyrinth** is a game that requires dexterity, comprehension of the physics interactions and being able to orient oneself in a 2D Space in order to mastering its gameplay.

## Controls

The game is pretty simple, and so are its controls. **The Labyrinth** can be played using only the Keyboard, and the player has access to **4** different actions, each one binded to a unique key: the <kbd>W</kbd> and <kbd>S</kbd> keys will let the player tilt the Maze **forward** and **backward**, where the <kbd>A</kbd> and <kbd>D</kbd> keys will let it tilts **to the left** and **to the right**.

In addition to controling the inclination of the playground, the player can also uses two more keys to controls the camera : the <kbd>Q</kbd> key will allow it **to zoom out with the camera**, in order to get a better look at the path it needs to take in the Maze. Once done, it can uses the <kbd>E</kbd> key if it wishes **to zoom in again on the ball** and its direct surrounding - like this red hole just there !

## Content

The Labyrinth features **15 different levels** with an increasing difficulty until its climax. These levels were not handcrafted inside the Unity engine: instead, they are generated "on the fly" during playtime, thanks to a script using a simple text-format to create game elements such as floors, walls and holes - the philosophy behind it being that each level or labyrinth can be seen as a checkerboard, with floor and wall made of cases of equal dimensions.
