using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class UIMenuManager : MonoBehaviour
{
    [SerializeField]
    public GameObject MenuPC = null;
    public GameObject MenuWeb = null;
    public VersionManager versionManager = null;

    void Start()
    {
      // Getting platform
      if (versionManager == null) versionManager = FindObjectOfType<VersionManager>();
      // Activation of the right menu for the targeted platform
      switch (versionManager.platform)
      {
        case "PC":
          if (MenuPC != null) MenuPC.SetActive(true);
          break;
        case "Web":
          if (MenuWeb != null) MenuWeb.SetActive(true);
          break;
        default:
          if (MenuPC != null) MenuPC.SetActive(true);
          break;
      }
    }

    /* ----------------------- */
    /*      Button Methods     */
    /* ----------------------- */
    public void _LoadOptions()
    {
      SceneManager.LoadScene("Options");
  	}

    public void _StartGame()
    {
      SceneManager.LoadScene("Selection");
    }

    public void _StartTutorial()
    {
      FindObjectOfType<GameManager>().SetState(FindObjectOfType<GameManager>().stateGame);
  		SceneManager.LoadScene("Tutorial");
    }

    public void _QuitGame()
    {
      Application.Quit();
    }
}
