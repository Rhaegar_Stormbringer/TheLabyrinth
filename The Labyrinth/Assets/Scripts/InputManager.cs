using System;
﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InputManager : MonoBehaviour
{
  public static InputManager instance;

  public Dictionary<string, Dictionary<string, KeyCode>> buttonKeys = new Dictionary<string, Dictionary<string, KeyCode>>();
  //public Keybindings keybindings;

  void Awake()
  {
    if (instance == null)
    {
      instance = this;
      DontDestroyOnLoad(this);
    }
    else if (instance != this)
    {
      Destroy(this);
    }
  }

  public string[] GetButtonNames()
  {
    return buttonKeys.Keys.ToArray();
  }


  public string[] GetButtonValues()
  {
    List<string> s = new List<string>();
    foreach (var action in buttonKeys)
    {
      foreach (var type in action.Value)
      {
        if (type.Key == "primary")
        {
          s.Add(type.Value.ToString());
        }
      }
    }
    return s.ToArray();
  }

  public override string ToString()
  {
    string s = "";
    foreach (var action in buttonKeys)
    {
      s += action.Key+"\n";
      foreach (var type in action.Value)
      {
        s += type.Key+"|"+type.Value.ToString()+"\n";
      }
    }
    return s;
  }

  public void SetButtonValue(string buttonName, KeyCode keycode)
  {
    buttonKeys[buttonName]["primary"] = keycode;
  }

  public KeyCode CheckKey(string key, string type)
  {
    if (buttonKeys.ContainsKey(key))
    {
      return buttonKeys[key][type];
    }
    else
    {
      return KeyCode.None;
    }
  }

  public bool Key(string key)
  {
    if (Input.GetKey(CheckKey(key, "primary")) || Input.GetKey(CheckKey(key, "secondary")))
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  public void LoadDefaultBinding()
  {
    buttonKeys["up"] = new Dictionary<string, KeyCode>();
    buttonKeys["up"]["primary"] = KeyCode.W;
    buttonKeys["up"]["secondary"] = KeyCode.UpArrow;
    buttonKeys["down"] = new Dictionary<string, KeyCode>();
    buttonKeys["down"]["primary"] = KeyCode.S;
    buttonKeys["down"]["secondary"] = KeyCode.DownArrow;
    buttonKeys["left"] = new Dictionary<string, KeyCode>();
    buttonKeys["left"]["primary"] = KeyCode.A;
    buttonKeys["left"]["secondary"] = KeyCode.LeftArrow;
    buttonKeys["right"] = new Dictionary<string, KeyCode>();
    buttonKeys["right"]["primary"] = KeyCode.D;
    buttonKeys["right"]["secondary"] = KeyCode.RightArrow;
    buttonKeys["zoom"] = new Dictionary<string, KeyCode>();
    buttonKeys["zoom"]["primary"] = KeyCode.E;
    buttonKeys["zoom"]["secondary"] = KeyCode.None;
    buttonKeys["dezoom"] = new Dictionary<string, KeyCode>();
    buttonKeys["dezoom"]["primary"] = KeyCode.Q;
    buttonKeys["dezoom"]["secondary"] = KeyCode.None;
  }

  public void LoadBinding(string up, string down, string left, string right, string zoom, string dezoom)
  {
    try
    {
      buttonKeys["up"] = new Dictionary<string, KeyCode>();
      buttonKeys["up"]["primary"] = (KeyCode) System.Enum.Parse(typeof(KeyCode), up);
      buttonKeys["up"]["secondary"] = KeyCode.UpArrow;
      buttonKeys["down"] = new Dictionary<string, KeyCode>();
      buttonKeys["down"]["primary"] = (KeyCode) System.Enum.Parse(typeof(KeyCode), down);
      buttonKeys["down"]["secondary"] = KeyCode.DownArrow;
      buttonKeys["left"] = new Dictionary<string, KeyCode>();
      buttonKeys["left"]["primary"] = (KeyCode) System.Enum.Parse(typeof(KeyCode), left);
      buttonKeys["left"]["secondary"] = KeyCode.LeftArrow;
      buttonKeys["right"] = new Dictionary<string, KeyCode>();
      buttonKeys["right"]["primary"] = (KeyCode) System.Enum.Parse(typeof(KeyCode), right);
      buttonKeys["right"]["secondary"] = KeyCode.RightArrow;
      buttonKeys["zoom"] = new Dictionary<string, KeyCode>();
      buttonKeys["zoom"]["primary"] = (KeyCode) System.Enum.Parse(typeof(KeyCode), zoom);
      buttonKeys["zoom"]["secondary"] = KeyCode.None;
      buttonKeys["dezoom"] = new Dictionary<string, KeyCode>();
      buttonKeys["dezoom"]["primary"] = (KeyCode) System.Enum.Parse(typeof(KeyCode), dezoom);
      buttonKeys["dezoom"]["secondary"] = KeyCode.None;
    }
    catch (ArgumentException)
    {
      LoadDefaultBinding();
    }
  }
}
