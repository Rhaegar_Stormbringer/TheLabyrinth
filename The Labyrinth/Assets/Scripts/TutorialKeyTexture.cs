﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialKeyTexture : MonoBehaviour
{
    [SerializeField]
    public string keyword;
    [SerializeField]
    public Texture2D[] textures;
    public string fileName;
    public string fileExtension;

    public KeyCode associatedKey;
    public Renderer m_Renderer;

    // Start is called before the first frame update
    void Start()
    {
        associatedKey = FindObjectOfType<InputManager>().buttonKeys[keyword]["primary"];
        string textureFile = fileName.Replace("%", associatedKey.ToString());
        m_Renderer = GetComponent<Renderer>();
        foreach(Texture2D tex in textures)
        {
          if (tex.name ==textureFile)
          {
            m_Renderer.material.SetTexture("_MainTex", tex);
            break;
          }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
