using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
  public static GameManager instance;
  [SerializeField]
  private string state = "menu";
  // Definition of the different possible states
  [SerializeField]
  public string stateMenu = "menu";
  public string stateGame = "game";
  public string stateGamePaused = "pause";

  [SerializeField]
  private string signal = "none";
  // Definition of the different signals that can be received
  [SerializeField]
  public string signalPause = "pause";
  public string signalResume = "resume";
  public string signalGameOver = "game over";
  public string signalVictory = "victory";
  public string signalBackToMenu = "back to menu";

  private Vector3 cameraOffset = new Vector3(0f, 10f, 0f);

  public string GetState()
  {
    return state;
  }

  public void SetState(string s)
  {
    state = s;
  }

  //Used instead of Update, for performance reasons
  private void CheckSignal()
  {
    switch (signal)
    {
      case "pause":
        SetState(stateGamePaused);
        Time.timeScale = 0;
        FindObjectOfType<UIGameManager>().Pause();
        break;
      case "resume":
        SetState(stateGame);
        FindObjectOfType<UIGameManager>().Resume();
        Time.timeScale = 1;
        break;
      case "game over":
        FindObjectOfType<UIGameManager>().GameOver();
        break;
      case "victory":
        FindObjectOfType<UIGameManager>().Victory();
        break;
      case "back to menu":
        SetState(stateMenu);
        Time.timeScale = 1;
        break;
      default:
        break;
    }
    ResetSignal();
  }

  public void SetSignal(string s)
  {
    signal = s;
    CheckSignal();
  }

  public void ResetSignal()
  {
    signal = "none";
  }

  public void SaveCameraOffset(Vector3 co)
  {
    cameraOffset = co;
  }

  public Vector3 GetCameraOffset()
  {
    return cameraOffset;
  }

  void Awake()
  {
    if (instance == null)
    {
      instance = this;
      DontDestroyOnLoad(this);
    }
    else if (instance != this)
    {
      Destroy(this);
    }
  }
}
