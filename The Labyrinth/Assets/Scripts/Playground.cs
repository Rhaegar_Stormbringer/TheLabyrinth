﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playground : MonoBehaviour
{
  public float speedX = 25f;
	public float speedZ = 25f;
	private float rotationX;
	public float minX = -5f;
	public float maxX = 5;
	private float rotationZ;
	public float minZ = -5f;
	public float maxZ = 5f;

  // Start is called before the first frame update
  void Start()
  {
    rotationX = transform.localRotation.eulerAngles.x;
    rotationZ = transform.localRotation.eulerAngles.z;
  }

  // Update is called once per frame
  void Update()
  {
    Quaternion localRotation = Quaternion.Euler(rotationX, 0f, rotationZ);
		transform.rotation = localRotation;
  }

  // Using Input Manager
  public void Rotate(bool up, bool down, bool left, bool right)
  {
    float inputX = 0.0f;
    float inputZ = 0.0f;

    if (up)
    {
      inputX = 1.0f;
    }
    else if (down)
    {
      inputX = -1.0f;
    }
    if (left)
    {
      inputZ = -1.0f;
    }
    else if (right)
    {
      inputZ = 1.0f;
    }

    rotationX += inputX * speedX * Time.deltaTime;
    rotationX = Mathf.Clamp(rotationX, minX, maxX);

    rotationZ -= inputZ * speedZ * Time.deltaTime;
    rotationZ = Mathf.Clamp(rotationZ, minZ, maxZ);
  }
}
