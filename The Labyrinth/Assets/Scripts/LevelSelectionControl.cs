﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectionControl : MonoBehaviour
{
	[SerializeField]
	private GameObject buttonPrefab = null;

	void Start()
	{
		// Get all level files
		LevelWatcherClass.allLevelFiles = Directory.GetFiles(Application.streamingAssetsPath, "*.txt");
		// Get all level preview picture files
		Regex reg = new Regex(@"^(?!.*DEFAULT)(?!.*_p).*$");
		LevelWatcherClass.allPreviewFiles = Directory.GetFiles(Application.streamingAssetsPath, "*.png").Where(path => reg.IsMatch(path)).ToArray();
		// Get all level preview grey picture (aka not completed) files
		reg = new Regex(@"^(?!.*DEFAULT).*_p.*$");
		LevelWatcherClass.allPreviewFiles_p = Directory.GetFiles(Application.streamingAssetsPath, "*.png").Where(path => reg.IsMatch(path)).ToArray();
		reg = new Regex(@"^.*DEFAULT.*$");
		LevelWatcherClass.defaultPreviewFiles = Directory.GetFiles(Application.streamingAssetsPath, "*.png").Where(path => reg.IsMatch(path)).ToArray();

		//Debug.Log(LevelWatcherClass.defaultPreviewFiles);
		//foreach (string s in LevelWatcherClass.defaultPreviewFiles)
		//{
		//	Debug.Log(s);
		//}
		int i = 0;
		int j = 0; // j is for desactivating the buttons of the level not yet completed, minus the first one
		foreach (string f in LevelWatcherClass.allLevelFiles)
		{
			//Debug.Log(f);
			GameObject button = Instantiate(buttonPrefab) as GameObject;
			button.SetActive(true);
			button.GetComponent<LevelSelectionButton>().SetIndex(i);
			//button.transform.parent = GameObject.FindGameObjectWithTag("WebLevelSelection_levelList").transform;
			button.transform.SetParent(buttonPrefab.transform.parent, false);

			// Delete the Application.streamingAssetsPath and the ".txt" for the path, to only keep the level's name
			string button_name = Regex.Match(f, @"^.*\\[0-9]*_(.{1,10}).*(?=.txt)", RegexOptions.Singleline).Groups[1].Value;
			//Debug.Log("[NAME] "+button_name);
			button.GetComponent<LevelSelectionButton>().SetText(button_name);

			// Check if a preview image exists for the level : if yes, apply it (normal or grey version depending of the completion state of the level)
			// If not, apply the default image preview (again, either normal or grey version)
			if (LevelWatcherClass.allLevelsCompleted.Contains(f))
			{
				int k_ok = 0;
				button.GetComponent<LevelSelectionButton>().SetLevelState(true);
				foreach (string s in LevelWatcherClass.allPreviewFiles)
				{
					string button_preview = Regex.Match(s, @"^.*\\[0-9]*_(.{1,10}).*(?=.png)", RegexOptions.Singleline).Groups[1].Value;
					if (button_preview == button_name)
					{
						button.GetComponent<LevelSelectionButton>().SetImage(s);
						k_ok = 1;
						break;
					}
				}
				if (k_ok == 0)
				{
					button.GetComponent<LevelSelectionButton>().SetImage(LevelWatcherClass.defaultPreviewFiles[0]);
				}

			}
			else
			{
				int k_ok = 0;
				button.GetComponent<LevelSelectionButton>().SetLevelState(false);
				if (j < 1)
				{
					foreach (string s in LevelWatcherClass.allPreviewFiles)
					{
						string button_preview = Regex.Match(s, @"^.*\\[0-9]*_(.{1,10}).*(?=.png)", RegexOptions.Singleline).Groups[1].Value;
						if (button_preview == button_name)
						{
							button.GetComponent<LevelSelectionButton>().SetImage(s);
							k_ok = 1;
							break;
						}
					}
					if (k_ok == 0)
					{
						button.GetComponent<LevelSelectionButton>().SetImage(LevelWatcherClass.defaultPreviewFiles[0]);
					}
				}
				else
				{
					foreach (string s in LevelWatcherClass.allPreviewFiles_p)
					{
						string button_preview_p = Regex.Match(s, @"^.*\\[0-9]*_(.{1,10})_.*(?=.png)", RegexOptions.Singleline).Groups[1].Value;
						//Debug.Log("[PREVIEW_P] "+button_name);
						if (button_preview_p == button_name)
						{
							//Debug.Log("oui");
							button.GetComponent<LevelSelectionButton>().SetImage(s);
							k_ok = 1;
							break;
						}
					}
					if (k_ok == 0)
					{
						button.GetComponent<LevelSelectionButton>().SetImage(LevelWatcherClass.defaultPreviewFiles[1]);
					}
				}
				j += 1;
			}
			if (j > 1)
			{
				button.GetComponent<Button>().interactable = false;
			}

			i+= 1;
		}
	}
}
