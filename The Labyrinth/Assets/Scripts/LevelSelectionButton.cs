﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelSelectionButton : MonoBehaviour
{
	public int index;
	
	[SerializeField]
	private Text buttonText = null;
	
	[SerializeField]
	private Image buttonPreview  = null;
	
	[SerializeField]
	private Image levelState  = null;
	
	public Sprite levelState_completed  = null;
	public Sprite levelState_notCompleted  = null;
	
	public void SetIndex(int i)
	{
		index = i;
	}
	
	public void SetText(string t)
	{
		buttonText.text = t;
	}
	
	public void SetLevelState(bool completed)
	{
		if (completed)
		{
			levelState.sprite = levelState_completed;
		}
		else
		{
			levelState.sprite = levelState_notCompleted;
		}
	}
	
	public void SetImage(string path)
	{
		Texture2D tex = new Texture2D(2, 2);
		byte[] fileData = File.ReadAllBytes(path);
		tex.LoadImage(fileData);
		Sprite s = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
		buttonPreview.sprite = s;
	}
	
    public void SelectLevel()
    {
		LevelWatcherClass.levelSelected = LevelWatcherClass.allLevelFiles[index];
		LevelWatcherClass.musicName = "game";
		LevelWatcherClass.changeMusic = 2;
        SceneManager.LoadScene("Level");
    }
	
	public void OnClick()
	{
		LevelWatcherClass.levelSelectedIndex = index;
		LevelWatcherClass.levelSelected = LevelWatcherClass.allLevelFiles[index];
		LevelWatcherClass.musicName = "game";
		LevelWatcherClass.changeMusic = 2;
		//Debug.Log(LevelWatcherClass.levelSelectedIndex);
		SceneManager.LoadScene("Level");
	}
}
