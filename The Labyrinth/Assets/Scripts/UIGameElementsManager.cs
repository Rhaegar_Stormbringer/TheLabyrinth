using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class UIGameElementsManager : MonoBehaviour
{
    [SerializeField]
    public GameObject UIPause = null;
    public GameObject UIPauseOptions = null;
    public GameObject UIPauseVolume = null;
    public GameObject UIGameOver = null;
    public GameObject UIVictory = null;
    public GameObject UIFinalVictory = null;
    // Start is called before the first frame update
    void Start()
    {
      // Safety deactivation
      if (UIPause != null) UIPause.SetActive(false);
      if (UIPauseVolume != null) UIPauseVolume.SetActive(false);
      if (UIGameOver != null) UIGameOver.SetActive(false);
      if (UIVictory != null) UIVictory.SetActive(false);
      if (UIFinalVictory != null) UIFinalVictory.SetActive(false);
    }

    /* ----------------------- */
    /*     Wrapper Methods     */
    /* ----------------------- */
    public void Pause()
    {
      if (UIPause != null) UIPause.SetActive(true);
    }

    public void GameOver()
    {
      if (UIGameOver != null) UIGameOver.SetActive(true);
    }

    public void Resume()
    {
      UIPauseOptions.SetActive(true);
      UIPauseVolume.SetActive(false);
      if (UIPause != null) UIPause.SetActive(false);
    }

    // General cases
    public void Victory()
    {
      switch(FindObjectOfType<LevelManager>().levelSelectedType)
      {
      case "campaign":
        VictoryCampaign();
        break;
      case "speedrun":
        break;
      case "mod":
        break;
      default:
        if (UIVictory != null) UIVictory.SetActive(true);
        break;
      }
    }

    // Used for the campaign in order to display a different UI depending on the level cleared
    public void VictoryCampaign()
    {
      try
		  {
        FindObjectOfType<LevelManager>().levelSelected = FindObjectOfType<LevelManager>().campaignLevels[FindObjectOfType<LevelManager>().levelSelectedIndex+1];
        if (UIVictory != null) UIVictory.SetActive(true);
      }
		  catch (System.IndexOutOfRangeException)
		  {
        FinalVictory();
      }
    }

    public void FinalVictory()
    {
      if (UIFinalVictory != null) UIFinalVictory.SetActive(true);
    }

    /* ----------------------- */
    /*      Button Methods     */
    /* ----------------------- */
    public void _BackToMenu()
    {
      FindObjectOfType<GameManager>().SetSignal(FindObjectOfType<GameManager>().signalBackToMenu);
      SceneManager.LoadScene("Menu");
    }

    public void _Endgame()
    {
      FindObjectOfType<GameManager>().SetSignal(FindObjectOfType<GameManager>().signalBackToMenu);
      SceneManager.LoadScene("Endgame");
    }

    public void _Next()
    {
      FindObjectOfType<LevelManager>().levelSelectedIndex += 1;
      switch(FindObjectOfType<LevelManager>().levelSelectedType)
  		{
  			case "campaign":
          FindObjectOfType<LevelManager>().levelSelected = FindObjectOfType<LevelManager>().campaignLevels[FindObjectOfType<LevelManager>().levelSelectedIndex];
  				break;
  			case "speedrun":
  				break;
  			case "mod":
          FindObjectOfType<LevelManager>().levelSelected = FindObjectOfType<LevelManager>().modLevels[FindObjectOfType<LevelManager>().levelSelectedIndex];
  				break;
  			default:
  				break;
  		}
      SceneManager.LoadScene("Level");
    }

    public void _Options()
    {
      UIPauseOptions.SetActive(false);
      UIPauseVolume.SetActive(true);
  	}

    public void _Resume()
    {
      FindObjectOfType<GameManager>().SetSignal(FindObjectOfType<GameManager>().signalResume);
      Resume();
    }

    public void _Retry()
    {
      FindObjectOfType<GameManager>().SetSignal(FindObjectOfType<GameManager>().signalResume);
      SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void _QuitGame()
    {
      Application.Quit();
    }
}
