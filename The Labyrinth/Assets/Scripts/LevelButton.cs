﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelButton : MonoBehaviour
{
	public int index;
	[SerializeField]
	private Text buttonText;
	[SerializeField]
	private Image buttonPreview;
	[SerializeField]
	private Image levelState;
	public Sprite levelCompleted;
	public Sprite levelNotCompleted;

	public void SetIndex(int i)
	{
		index = i;
	}

	public void SetText(string t)
	{
		buttonText.text = t;
	}

	public void SetLevelState(bool completed)
	{
		if (completed)
		{
			levelState.sprite = levelCompleted;
		}
		else
		{
			levelState.sprite = levelNotCompleted;
		}
	}

	public void SetImage(Texture2D tex)
	{
		buttonPreview.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
	}
}
