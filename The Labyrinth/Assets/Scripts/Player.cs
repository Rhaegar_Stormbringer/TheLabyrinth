﻿using UnityEngine;

public class Player : MonoBehaviour
{
  public Rigidbody rb;
	public float verticalForce;
	private GameObject _camera;
	private GameObject _playground;

  // Start is called before the first frame update
  void Start()
  {
  	rb = gameObject.GetComponent<Rigidbody>();
  	verticalForce = 2000f;
  	// Set camera
  	_camera = GameObject.FindWithTag("MainCamera");
  	_camera.GetComponent<Camera_>().player = transform;
  	// Set playground
  	_playground = GameObject.FindWithTag("Playground");
  }

  // Update is called once per frame
  void FixedUpdate()
  {
    // If not dead
    if (gameObject != null)
		{
			// Force the ball to stay in the Playground - avoid little undesired jumps
			rb.AddForce(0, -verticalForce * Time.deltaTime, 0);
			// Player Inputs
			_playground.GetComponent<Playground>().Rotate(FindObjectOfType<InputManager>().Key("up"),
                                                          FindObjectOfType<InputManager>().Key("down"),
                                                          FindObjectOfType<InputManager>().Key("left"),
                                                          FindObjectOfType<InputManager>().Key("right"));
      _camera.GetComponent<Camera_>().Zoom(FindObjectOfType<InputManager>().Key("zoom"),
                                                FindObjectOfType<InputManager>().Key("dezoom"));
		}
  }

  void Update()
  {
    if (gameObject != null)
		{
      if(FindObjectOfType<GameManager>().GetState() == FindObjectOfType<GameManager>().stateGame)
			{
        // Pausing
        if (Input.GetKeyDown(KeyCode.Escape))
        {
          FindObjectOfType<GameManager>().SetSignal(FindObjectOfType<GameManager>().signalPause);
        }
      }
      else if(FindObjectOfType<GameManager>().GetState() == FindObjectOfType<GameManager>().stateGamePaused)
			{
        // Pausing
        if (Input.GetKeyDown(KeyCode.Escape))
        {
          FindObjectOfType<GameManager>().SetSignal(FindObjectOfType<GameManager>().signalResume);
        }
      }
    }
  }
}
