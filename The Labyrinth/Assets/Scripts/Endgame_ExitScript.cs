﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Endgame_ExitScript : MonoBehaviour
{
	[SerializeField]
	private GameObject UI = null;

	public float force = 0.5f;
	public float radius = 50f;
	public float upForce = 0f;


    void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			// Destroy player's ball
			Destroy(other.gameObject);

			// Make the explosion at the exit
			Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
			foreach (Collider hit in colliders)
			{
				// Need to check this for exit and hole: non-convex MeshCollider with non-kinematic Rigidbody is no longer supported since Unity 5
				MeshCollider mc = hit.GetComponent<MeshCollider>();
				if (mc != null)
				{
					mc.convex = true;
				}

				Rigidbody rb = hit.GetComponent<Rigidbody>();
				if (rb != null)
				{
					rb.isKinematic = false;
					rb.AddExplosionForce(force, transform.position, radius, upForce, ForceMode.Impulse);
				}
			}

			UI.GetComponent<Endgame_UIScript>().ShowCongrats();
			FindObjectOfType<AudioManager>().Play(LevelWatcherClass.musicName);
            //FindObjectOfType<AudioManager>().StopMusic();
            //FindObjectOfType<AudioManager>().PlaySound("victory");
        }
	}
}
