using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class UISelectionManager : MonoBehaviour
{
  [SerializeField]
  public GameObject MenuSelectionPC = null;
  public GameObject DescriptionTitlePC = null;
  public GameObject DescriptionPC = null;
  public GameObject MenuSelectionWeb = null;
  public GameObject DescriptionTitleWeb = null;
  public GameObject DescriptionWeb = null;
  public VersionManager versionManager = null;

  void Start()
  {
    // Getting platform
    if (versionManager == null) versionManager = FindObjectOfType<VersionManager>();
    // Activation of the right menu for the targeted platform
    switch (versionManager.platform)
    {
        case "PC":
          if (MenuSelectionPC != null) MenuSelectionPC.SetActive(true);
          break;
        case "Web":
          if (MenuSelectionWeb != null) MenuSelectionWeb.SetActive(true);
          break;
        default:
          if (MenuSelectionPC != null) MenuSelectionPC.SetActive(true);
          break;
    }
    // Safety reset of description texts
    _ResetDescription();
  }

  /* ----------------------- */
  /*      Button Methods     */
  /* ----------------------- */
  public void _BackToMenu()
  {
    SceneManager.LoadScene("Menu");
  }

  public void _ChoiceCampaign()
  {
    SceneManager.LoadScene("SelectionCampaign");
  }

  public void _ChoiceSpeedrun()
  {
    Debug.Log("Speedrun");
  }

  public void _ChoiceMod()
  {
    Debug.Log("Mod");
  }

  public void _ShowDescription(Button button)
  {
    switch (versionManager.platform)
    {
        case "PC":
          if (DescriptionTitlePC != null) DescriptionTitlePC.GetComponent<Text>().text = button.name;
          if (DescriptionPC != null) DescriptionPC.GetComponent<Text>().text = button.GetComponent<TextDescription>().Text.text;
          break;
        case "Web":
          if (DescriptionTitleWeb != null) DescriptionTitleWeb.GetComponent<Text>().text = button.name;
          if (DescriptionWeb != null) DescriptionWeb.GetComponent<Text>().text = button.GetComponent<TextDescription>().Text.text;
          break;
        default:
          if (DescriptionTitlePC != null) DescriptionTitlePC.GetComponent<Text>().text = button.name;
          if (DescriptionPC != null) DescriptionPC.GetComponent<Text>().text = button.GetComponent<TextDescription>().Text.text;
          break;
    }
  }

  public void _ResetDescription()
  {
    switch (versionManager.platform)
    {
        case "PC":
          if (DescriptionTitlePC != null) DescriptionTitlePC.GetComponent<Text>().text = "";
          if (DescriptionPC != null) DescriptionPC.GetComponent<Text>().text = "";
          break;
        case "Web":
          if (DescriptionTitleWeb != null) DescriptionTitleWeb.GetComponent<Text>().text = "";
          if (DescriptionWeb != null) DescriptionWeb.GetComponent<Text>().text = "";
          break;
        default:
          if (DescriptionTitlePC != null) DescriptionTitlePC.GetComponent<Text>().text = "";
          if (DescriptionPC != null) DescriptionPC.GetComponent<Text>().text = "";
          break;
    }
  }
}
