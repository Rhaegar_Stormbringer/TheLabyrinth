using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VersionManager : MonoBehaviour
{
  public static VersionManager instance;
  [SerializeField]
  public string platform;

  void Awake()
  {
    if (instance == null)
    {
      instance = this;
      DontDestroyOnLoad(this);
    }
    else if (instance != this)
    {
      Destroy(this);
    }
  }
}
