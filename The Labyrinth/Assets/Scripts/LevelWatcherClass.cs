﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LevelWatcherClass
{
	public static SaveManager sm;
	public static AudioManager am;
	public static int changeMusic = 0; // 0 = no music so play music | 1 = don't change music on load | 2 = change music on load
	public static string musicName = "menu";
	
	public static string[] allLevelFiles;	// PATH ! to files
	public static string[] allPreviewFiles;	// PATH ! to files
	public static string[] allPreviewFiles_p;	// PATH ! to files
	public static string[] defaultPreviewFiles;
	public static string levelSelected;
	public static int levelSelectedIndex;
	
	public static List<string> allLevelsCompleted = new List<string>();
}
