﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
	//private string path = "Assets/Levels/";
	public TextAsset levelFile;

	private GameObject parent = null;	// empty GameObject used as parent for all the playground
	private GameObject child = null;		// used as iterator on the created prefabs to link them to the playground parent empty GameObject

	public Transform Void;
	public Transform Floor;
	public Transform Wall;
	public Transform Exit;
	public Transform Hole;
	public Transform Ball;
	public Transform SafetyNet;

	public const string voidToken = ".";
	public const string floorToken = "0";
	public const string wallToken = "1";
	public const string exitToken = "X";
	public const string holeToken = "D";
	public const string ballToken = "S";

	public float scaling;	// Applied in case of sacle differences between prefabs (such as the ball and the floor)

	private List<GameObject> childList = new List<GameObject>();
	private float[] sumCoords = {0f, 0f, 0f};

	void Start()
	{
		switch (FindObjectOfType<LevelManager>().levelSelectedType)
		{
			case "campaign":
				GenerateCampaignLevel();
				break;
			case "speedrun":
				break;
			case "mod":
				GenerateModLevel();
				break;
			default:
				break;
		}
	}



	void GenerateCampaignLevel()
	{
		Debug.Log($"Level: {FindObjectOfType<LevelManager>().levelSelected.name}");
		levelFile = FindObjectOfType<LevelManager>().levelSelected;
		// Create the parent empty GameObject and apply the script to rotate the playground to it
		parent = GameObject.FindWithTag("Playground");
		string content = levelFile.text;
		string[] lines = Regex.Split(content, "\r\n|\r|\n");
		int rows = lines.Length;
		string[][] levelContent = new string[rows][];
		for (int i = 0; i < lines.Length; i++)
		{
			string[] line = Regex.Split(lines[i], " ");
			levelContent[i] = line;
		}
		// Construct the level with prefabs based on the level textfile content
		for (int x = 0; x < levelContent.Length; x++)
		{
			for (int y = 0; y < levelContent[x].Length; y++)
			{
				switch (levelContent[x][y])
				{
					case voidToken:
						createAsset(Void, x, y, 0f);
						break;
					case floorToken:
						createAsset(Floor, x, y, 0f);
						break;
					case wallToken:
						createAsset(Floor, x, y, 0f);
						createAsset(Wall, x, y, 1.5f);
						break;
					case exitToken:
						createAsset(Exit, x, y, 0f);
						break;
					case holeToken:
						createAsset(Hole, x, y, 0f);
						break;
					case ballToken:
						createAsset(Floor, x, y, 0f);
						createAsset(Ball, x, y, 1f);
						break;
					default:
						break;
				}
			}
		}
		// Create the Safety Net under the level and resize it to cover the whole area
		createAsset(SafetyNet, 0, 0, -0.8f);
		// Replace all the prefabs in order to center the whole level around (0, 0, 0)
		foreach(var child in childList)
		{
			child.transform.position -= new Vector3(sumCoords[0]/2f, sumCoords[1]/2f, sumCoords[2]/2f);
			// set parent to Playground
			child.transform.SetParent(parent.transform);
			if (child.tag == "Player")
			{
				// Center the camera on the ball and set its max vertical offset based on the size of the level
				GameObject camera = GameObject.FindWithTag("MainCamera");
				camera.GetComponent<Camera_>().player = child.transform;
				camera.GetComponent<Camera_>().maxOffsetY = sumCoords[0];
			}
			if (child.tag == "SafetyNet")
			{
				child.transform.position = new Vector3(0, -1f, 0);
				child.transform.localScale = new Vector3(sumCoords[0]*2, 1f, sumCoords[2]*2);
			}
		}
	}



	void GenerateModLevel()
  {
		Debug.Log($"Level: {FindObjectOfType<LevelManager>().levelSelected.name}");
		// Create the parent empty GameObject and apply the script to rotate the playground to it
		parent = GameObject.FindWithTag("Playground");
		// Read the Level File, returning a 2D array containing all its splitted information
		string content = System.IO.File.ReadAllText(LevelWatcherClass.levelSelected);
		string[] lines = Regex.Split(content, "\r\n|\r|\n");
		int rows = lines.Length;
		string[][] levelContent = new string[rows][];
		for (int i = 0; i < lines.Length; i++)
		{
			string[] line = Regex.Split(lines[i], " ");
			levelContent[i] = line;
		}
		// Construct the level with prefabs based on the level textfile content
		for (int x = 0; x < levelContent.Length; x++)
		{
			for (int y = 0; y < levelContent[x].Length; y++)
			{
				switch (levelContent[x][y])
				{
					case voidToken:
						createAsset(Void, x, y, 0f);
						break;
					case floorToken:
						createAsset(Floor, x, y, 0f);
						break;
					case wallToken:
						createAsset(Floor, x, y, 0f);
						createAsset(Wall, x, y, 1.5f);
						break;
					case exitToken:
						createAsset(Exit, x, y, 0f);
						break;
					case holeToken:
						createAsset(Hole, x, y, 0f);
						break;
					case ballToken:
						createAsset(Floor, x, y, 0f);
						createAsset(Ball, x, y, 1f);
						break;
					default:
						break;
				}
			}
		}
		// Create the Safety Net under the level and resize it to cover the whole area
		createAsset(SafetyNet, 0, 0, -0.8f);
		// Replace all the prefabs in order to center the whole level around (0, 0, 0)
		foreach(var child in childList)
		{
			child.transform.position -= new Vector3(sumCoords[0]/2f, sumCoords[1]/2f, sumCoords[2]/2f);
			// set parent to Playground
			child.transform.SetParent(parent.transform);
			if (child.tag == "Player")
			{
				// Center the camera on the ball and set its max vertical offset based on the size of the level
				GameObject camera = GameObject.FindWithTag("MainCamera");
				camera.GetComponent<Camera_>().player = child.transform;
				camera.GetComponent<Camera_>().maxOffsetY = sumCoords[0];
			}
			if (child.tag == "SafetyNet")
			{
				child.transform.position = new Vector3(0, -1f, 0);
				child.transform.localScale = new Vector3(sumCoords[0]*2, 1f, sumCoords[2]*2);
			}
		}
	}



	void createAsset(Transform prefabs, int x, int y, float height)
	{
		// Prefabs.localScale.z because Y axis is the vertical axis
		child = Instantiate(prefabs, new Vector3(y*prefabs.localScale.x*(scaling/prefabs.localScale.x), height, -x*prefabs.localScale.z*(scaling/prefabs.localScale.z)), Quaternion.identity).gameObject;
		maxCoordsChecker(child.transform.position.x, child.transform.position.z);
		childList.Add(child);
	}

	void maxCoordsChecker(float positionX, float positionZ)
	{
		if (positionX > sumCoords[0])
		{
			sumCoords[0] = positionX;
		}
		if (positionZ < sumCoords[2])
		{
			sumCoords[2] = positionZ;
		}
	}
}
