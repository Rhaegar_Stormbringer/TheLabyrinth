using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class UIOptionsManager : MonoBehaviour
{
    [SerializeField]
    public GameObject MenuPC = null;
    public GameObject MenuWeb = null;
    public VersionManager versionManager = null;
    // Start is called before the first frame update
    void Start()
    {
      // Getting platform
      if (versionManager == null) versionManager = FindObjectOfType<VersionManager>();
      // Safety deactivation
      if (MenuPC != null) MenuPC.SetActive(false);
      if (MenuWeb != null) MenuWeb.SetActive(false);
      // Activation of the right menu for the targeted platform
      switch (versionManager.platform)
      {
        case "PC":
          if (MenuPC != null) MenuPC.SetActive(true);
          break;
        case "Web":
          if (MenuWeb != null) MenuWeb.SetActive(true);
          break;
        default:
          if (MenuPC != null) MenuPC.SetActive(true);
          break;
      }
    }

    public void BackToMenu()
    {
      SceneManager.LoadScene("Menu");
    }
}
