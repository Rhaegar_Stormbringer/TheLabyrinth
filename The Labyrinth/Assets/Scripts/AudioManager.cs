﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
	private static int _instance;

	[SerializeField]
	private string GAMESTATE = "menu";


	public int changeMusic = 0; // 0 = no music so play music | 1 = don't change music on load | 2 = change music on load
	public string musicName = "menu";
	public int STATE = 0;	// 0 = standby | 1 = playing

	private AudioSource audiosrc;
  public Sound[] sounds;


	private void Awake()
    {
		//Debug.Log(gameObject.GetInstanceID()+" | "+_instance);
        if (_instance != 0 && _instance != gameObject.GetInstanceID())
        {
            DestroyImmediate(this.gameObject);
        } else {
            _instance = gameObject.GetInstanceID();
			DontDestroyOnLoad(gameObject);
			audiosrc = GetComponent<AudioSource>();
        }
    }

	void Start()
	{
		//private
		//LevelWatcherClass.allLevelFiles = Directory.GetFiles(Application.streamingAssetsPath, "*.txt");
	}

	public void ChangeMusic()
	{
		Sound s = Array.Find(sounds, sound => sound.name == GAMESTATE);
		audiosrc.clip = s.clip;
	}

	public void ChangeMusic(string name)
	{
		Sound s = Array.Find(sounds, sound => sound.name == name);
		audiosrc.clip = s.clip;
	}

	public void PlayMusic()
    {
      if (!audiosrc.isPlaying)
			{
				audiosrc.loop = true;
				audiosrc.Play();
				STATE = 1;
			}
		}

	public void PlaySound(string name)
	{
		audiosrc.loop = false;
		audiosrc.PlayOneShot(Array.Find(sounds, sound => sound.name == name).clip);
		GAMESTATE = name;
	}

  public void StopMusic()
  {
		audiosrc.Stop();
		STATE = 0;
  }


	public void Play()
	{
		StopMusic();
		ChangeMusic();
		PlayMusic();
	}

	public void Play(string name)
	{
		switch (STATE)
		{
			case 0:
				ChangeMusic(name);
				PlayMusic();
				break;
			case 1:
				if (name != GAMESTATE)
				{
					StopMusic();
					ChangeMusic(name);
					PlayMusic();
					GAMESTATE = name;
				}
				break;
			default:
				break;
		}
	}
}
