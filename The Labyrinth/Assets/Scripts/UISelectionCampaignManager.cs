using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UISelectionCampaignManager : MonoBehaviour
{
  [SerializeField]
  public GameObject MenuPC = null;
  public GameObject MenuWeb = null;
  public VersionManager versionManager = null;

  [SerializeField]
  private GameObject buttonPrefab = null;

   // Used as a parent for the buttons
   // This is the UI object that going to be populated
  [SerializeField]
  private GameObject listPC = null;
  [SerializeField]
  private GameObject listWeb = null;

  void Start()
  {
    // Getting platform
    if (versionManager == null) versionManager = FindObjectOfType<VersionManager>();
    // Activation of the right menu for the targeted platform
    switch (versionManager.platform)
    {
        case "PC":
          if (MenuPC != null) MenuPC.SetActive(true);
          break;
        case "Web":
          if (MenuWeb != null) MenuWeb.SetActive(true);
          break;
        default:
          if (MenuPC != null) MenuPC.SetActive(true);
          break;
    }
    GenerateLevelsList();
  }

  void GenerateLevelsList()
  {
    int i = 0;
    int j = 0; // j is for desactivating the buttons of the level not yet completed, minus the first one
    foreach (TextAsset level in FindObjectOfType<LevelManager>().campaignLevels)
    {
      GameObject button = Instantiate(buttonPrefab) as GameObject;
      button.SetActive(true);
      button.GetComponent<LevelButton>().SetIndex(i);
      switch (versionManager.platform)
      {
          case "PC":
            if (listPC != null) button.transform.SetParent(listPC.transform);
            break;
          case "Web":
            if (listWeb != null) button.transform.SetParent(listWeb.transform);
            break;
          default:
            if (listPC != null) button.transform.SetParent(listPC.transform);
            break;
      }
      button.transform.localScale = new Vector3(1f, 1f, 1f);
      button.GetComponent<Button>().onClick.AddListener( () => {SelectLevel(button.GetComponent<LevelButton>().index);});

      // Delete the Application.streamingAssetsPath and the ".txt" for the path, to only keep the level's name
      string button_name = Regex.Match(level.name, @"[0-9]*_(.{1,10})", RegexOptions.Singleline).Groups[1].Value;
      button.GetComponent<LevelButton>().SetText(button_name);
      // Check if a preview image exists for the level : if yes, apply it (normal or grey version depending of the completion state of the level)
      // If not, apply the default image preview (again, either normal or grey version)
      if (FindObjectOfType<LevelManager>().levelsCompleted.Contains(level))
      {
        button.GetComponent<LevelButton>().SetLevelState(true);
        button.GetComponent<LevelButton>().SetImage(FindObjectOfType<LevelManager>().campaignPreviews[i]);
      }
      else
      {
        button.GetComponent<LevelButton>().SetLevelState(false);
        if (j < 1)
        {
          button.GetComponent<LevelButton>().SetImage(FindObjectOfType<LevelManager>().campaignPreviews[i]);

        }
        else
        {
          button.GetComponent<LevelButton>().SetImage(FindObjectOfType<LevelManager>().campaignPreviews_p[i]);
        }
        j += 1;
      }
      if (j > 1)
      {
        button.GetComponent<Button>().interactable = false;
      }
      i+= 1;
    }
  }

  /* ----------------------- */
  /*      Button Methods     */
  /* ----------------------- */
  public void _BackToSelection()
  {
    SceneManager.LoadScene("Selection");
  }

  public void SelectLevel(int index)
  {
    Debug.Log("INDEX : "+index);
    FindObjectOfType<LevelManager>().levelSelected = FindObjectOfType<LevelManager>().campaignLevels[index];
    FindObjectOfType<LevelManager>().levelSelectedType = "campaign";
    FindObjectOfType<LevelManager>().levelSelectedIndex = index;
    //FindObjectOfType<LevelManager>().musicName = "game";
    //FindObjectOfType<LevelManager>().changeMusic = 2;
    SceneManager.LoadScene("Level");
  }
}
