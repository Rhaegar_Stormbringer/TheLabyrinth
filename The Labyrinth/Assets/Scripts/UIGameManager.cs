using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class UIGameManager : MonoBehaviour
{
    [SerializeField]
    public GameObject UIGamePC = null;
    public GameObject UIGameWeb = null;
    public VersionManager versionManager = null;
    // Start is called before the first frame update
    void Start()
    {
      // Getting platform
      if (versionManager == null) versionManager = FindObjectOfType<VersionManager>();
      // Safety deactivation
      // UPDATE : These GameObjects MUST be activated: otherwise, the attached script will not be active,
      // which will prevent the associated UI elements from appearing.
      //if (UIGamePC != null) UIGamePC.active = false;
      //if (UIGameWeb != null) UIGameWeb.active = false;
    }

    public void Pause()
    {
      switch (versionManager.platform)
      {
        case "PC":
          UIGamePC.GetComponent<UIGameElementsManager>().Pause();
          break;
        case "Web":
          UIGameWeb.GetComponent<UIGameElementsManager>().Pause();
          break;
        default:
          UIGamePC.GetComponent<UIGameElementsManager>().Pause();
          break;
      }
    }

    public void GameOver()
    {
      switch (versionManager.platform)
      {
        case "PC":
          UIGamePC.GetComponent<UIGameElementsManager>().GameOver();
          break;
        case "Web":
          UIGameWeb.GetComponent<UIGameElementsManager>().GameOver();
          break;
        default:
          UIGamePC.GetComponent<UIGameElementsManager>().GameOver();
          break;
      }
    }

    public void Resume()
    {
      switch (versionManager.platform)
      {
        case "PC":
          UIGamePC.GetComponent<UIGameElementsManager>().Resume();
          break;
        case "Web":
          UIGameWeb.GetComponent<UIGameElementsManager>().Resume();
          break;
        default:
          UIGamePC.GetComponent<UIGameElementsManager>().Resume();
          break;
      }
    }

    public void Victory()
    {
      switch (versionManager.platform)
      {
        case "PC":
          UIGamePC.GetComponent<UIGameElementsManager>().Victory();
          break;
        case "Web":
          UIGameWeb.GetComponent<UIGameElementsManager>().Victory();
          break;
        default:
          UIGamePC.GetComponent<UIGameElementsManager>().Victory();
          break;
      }
    }
}
