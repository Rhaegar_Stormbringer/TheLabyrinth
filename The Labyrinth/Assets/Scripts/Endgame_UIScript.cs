﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Endgame_UIScript : MonoBehaviour
{
	[SerializeField]
	private CanvasGroup EndgameMenu = null;
	[SerializeField]
	private CanvasGroup SpeechMenu = null;
	[SerializeField]
	private CanvasGroup CreditsMenu = null;
	[SerializeField]
	private CanvasGroup ThanksMenu = null;
	[SerializeField]
	private CanvasGroup FinishItButton = null;
	[SerializeField]
	private GameObject Ball = null;

    // Start is called before the first frame update
    void Start()
    {
			HideCongrats();
			HideSpeech();
			HideCredits();
			HideThanks();
    }

	public void BackToMenu()
	{
		LevelWatcherClass.musicName = "menu";
		SceneManager.LoadScene("Menu");
	}

	public void FirstCredits()
	{
		HideSpeech();
		ShowCredits();
	}

	public void NextCredits()
	{
		HideCredits();
		ShowThanks();

	}
	public void HideCongrats()
	{
		EndgameMenu.alpha = 0f;
		EndgameMenu.interactable = false;
		EndgameMenu.blocksRaycasts = false;
	}

	private void HideCredits()
	{
		CreditsMenu.alpha = 0f;
		CreditsMenu.interactable = false;
		CreditsMenu.blocksRaycasts = false;

	}

	private void HideSpeech()
	{
		SpeechMenu.alpha = 0f;
		SpeechMenu.interactable = false;
		SpeechMenu.blocksRaycasts = false;
	}

	private void HideThanks()
	{
		ThanksMenu.alpha = 0f;
		ThanksMenu.interactable = false;
		ThanksMenu.blocksRaycasts = false;
	}

	public void ShowCongrats()
	{
		EndgameMenu.alpha = 1f;
		EndgameMenu.interactable = true;
		EndgameMenu.blocksRaycasts = true;
		ShowSpeech();
	}

	private void ShowCredits()
	{
		CreditsMenu.alpha = 1f;
		CreditsMenu.interactable = true;
		CreditsMenu.blocksRaycasts = true;

	}

	private void ShowSpeech()
	{
		SpeechMenu.alpha = 1f;
		SpeechMenu.interactable = true;
		SpeechMenu.blocksRaycasts = true;
	}

	private void ShowThanks()
	{
		ThanksMenu.alpha = 1f;
		ThanksMenu.interactable = true;
		ThanksMenu.blocksRaycasts = true;
	}

	public void StartEndgame()
	{
		FinishItButton.alpha = 0f;
		FinishItButton.interactable = false;
		FinishItButton.blocksRaycasts = false;
		Ball.GetComponent<Rigidbody>().isKinematic = false;
	}
}
