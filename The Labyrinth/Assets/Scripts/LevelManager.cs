using System;
﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
  public static LevelManager instance;

  [SerializeField]
  public TextAsset[] campaignLevels;
  public Texture2D[] campaignPreviews;
  public Texture2D[] campaignPreviews_p;

  public TextAsset[] modLevels;
  public Texture2D[] modPreviews;
  public Texture2D[] modPreviews_p;

  public Texture2D[] defaultPreviews;

  public TextAsset levelSelected;
  public string levelSelectedType;
  public int levelSelectedIndex;

  public List<TextAsset> levelsCompleted = new List<TextAsset>();

  void Awake()
  {
    if (instance == null)
    {
      instance = this;
      DontDestroyOnLoad(this);
    }
    else if (instance != this)
    {
      Destroy(this);
    }
  }
}
