﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_ : MonoBehaviour
{
  public Transform player;
	public float maxOffsetY = 15f;
	public float minOffsetY = 10f;
	private Vector3 offset;
	private float step = 0.1f;

  // Start is called before the first frame update
  void Start()
  {
    offset = FindObjectOfType<GameManager>().GetCameraOffset();
  }

  // Update is called once per frame
  void Update()
  {
    if (player != null)
		{
			transform.position = player.position + offset;
		}
  }

	// Camera zoom / dezoom ingame. Should takes into account the size of the level
  public void Zoom(bool inputZoom, bool inputDezoom)
	{
    if (inputDezoom && offset.y <= maxOffsetY)
	  {
		    offset += new Vector3(0f, step, 0f);
		}

		if (inputZoom && offset.y >= minOffsetY)
		{
			offset -= new Vector3(0f, step, 0f);
		}
    // Saving the offset - useful in cases of death
    FindObjectOfType<GameManager>().SaveCameraOffset(offset);
	}
}
