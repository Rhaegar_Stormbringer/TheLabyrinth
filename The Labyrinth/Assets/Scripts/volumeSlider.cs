﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class volumeSlider : MonoBehaviour
{
	[SerializeField]
	private AudioSource source = null;
	[SerializeField]
	private Image volumeImage = null;
	[SerializeField]
	private Sprite volumeImage_off = null;
	[SerializeField]
	private Sprite volumeImage_on = null;

	public void Start()
	{
		if (source == null)
		{
			source = FindObjectOfType<AudioManager>().GetComponent<AudioSource>();
			GetComponent<Slider>().value = source.volume;
		}
	}
	public void setVolume(float sliderValue)
	{
		source.volume = (float)sliderValue;
		if (source.volume == GetComponent<Slider>().minValue)
		{
			volumeImage.sprite = volumeImage_off;
		}
		else
		{
			volumeImage.sprite = volumeImage_on;
		}
		FindObjectOfType<SaveManager>().SaveOptions();
	}
}
