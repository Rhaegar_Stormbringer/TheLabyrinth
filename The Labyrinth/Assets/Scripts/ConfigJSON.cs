﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public class ConfigJSON
{
  public string volume = "";
  public string up = "";
  public string down = "";
  public string left = "";
  public string right = "";
  public string zoom = "";
  public string dezoom = "";

  public void Fills(string volume, string up, string down, string left, string right, string zoom, string dezoom)
  {
    this.volume = volume;
    this.up = up;
    this.down = down;
    this.left = left;
    this.right = right;
    this.zoom = zoom;
    this.dezoom = dezoom;
  }

  public void FillsDefault()
  {
    this.volume = "1";
    this.up = "W";
    this.down = "S";
    this.left = "A";
    this.right = "D";
    this.zoom = "E";
    this.dezoom = "Q";
  }

  public bool Check()
  {
    if (volume == "" || up == "" || down == "" || left == "" || right == "" || zoom == "" || dezoom == "")
    {
      return false;
    }
    return true;
  }

  public override string ToString()
  {
    return volume+" | "+up+" | "+down+" | "+left+" | "+right+" | "+zoom+" | "+dezoom;
  }
}
