using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class LevelSelectionCampaign : MonoBehaviour
{
  [SerializeField]
  //private GameObject buttonPrefab = null;

  // Start is called before the first frame update
  void Start()
  {
    /*
    int i = 0;
    int j = 0; // j is for desactivating the buttons of the level not yet completed, minus the first one
    foreach (TextAsset level in FindObjectOfType<LevelManager>().campaignLevels)
    {
      GameObject button = Instantiate(buttonPrefab) as GameObject;
      button.SetActive(true);
      button.GetComponent<LevelSelectionButton>().SetIndex(i);
      button.transform.SetParent(buttonPrefab.transform.parent, false);
      button.GetComponent<Button>().onClick.AddListener( () => {SelectLevel(button.GetComponent<WebLevelSelectionButton>().index);});

      // Delete the Application.streamingAssetsPath and the ".txt" for the path, to only keep the level's name
      string button_name = Regex.Match(level.name, @"[0-9]*_(.{1,10})", RegexOptions.Singleline).Groups[1].Value;
      button.GetComponent<LevelSelectionButton>().SetText(button_name);
      // Check if a preview image exists for the level : if yes, apply it (normal or grey version depending of the completion state of the level)
      // If not, apply the default image preview (again, either normal or grey version)
      if (FindObjectOfType<LevelManager>().levelsCompleted.Contains(level))
      {
        button.GetComponent<LevelSelectionButton>().SetLevelState(true);
        button.GetComponent<LevelSelectionButton>().SetImage(FindObjectOfType<LevelManager>().campaignPreviews[i]);
      }
      else
      {
        button.GetComponent<LevelSelectionButton>().SetLevelState(false);
        if (j < 1)
        {
          button.GetComponent<LevelSelectionButton>().SetImage(FindObjectOfType<LevelManager>().campaignPreviews[i]);

        }
        else
        {
          button.GetComponent<LevelSelectionButton>().SetImage(FindObjectOfType<LevelManager>().campaignPreviews_p[i]);
        }
        j += 1;
      }
      if (j > 1)
      {
        button.GetComponent<Button>().interactable = false;
      }
      i+= 1;
    }
    */
  }

  public void SelectLevel(int index)
  {
    Debug.Log("INDEX : "+index);
    /*
    FindObjectOfType<LevelManager>().levelSelected = FindObjectOfType<LevelManager>().levels[index];
    FindObjectOfType<LevelManager>().musicName = "game";
    FindObjectOfType<LevelManager>().changeMusic = 2;
    */
    SceneManager.LoadScene("WebLevel");
  }
}
