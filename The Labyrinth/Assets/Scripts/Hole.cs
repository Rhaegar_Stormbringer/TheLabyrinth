﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hole : MonoBehaviour
{
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			// Destroy player's ball
			Destroy(other.gameObject);
			FindObjectOfType<GameManager>().SetSignal(FindObjectOfType<GameManager>().signalGameOver);
		}
	}
}
