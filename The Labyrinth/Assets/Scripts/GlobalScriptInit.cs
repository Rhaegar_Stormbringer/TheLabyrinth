﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalScriptInit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		if (LevelWatcherClass.changeMusic == 0)
		{
			FindObjectOfType<AudioManager>().Play(LevelWatcherClass.musicName);
			LevelWatcherClass.changeMusic = 1;
		}
		else if (LevelWatcherClass.changeMusic == 2)
		{
			if (LevelWatcherClass.musicName == "game")
			{
				string music = LevelWatcherClass.musicName + Random.Range(1, 5);
				FindObjectOfType<AudioManager>().Play(music);
				LevelWatcherClass.changeMusic = 1;
			}
			else
			{
				FindObjectOfType<AudioManager>().Play(LevelWatcherClass.musicName);
				LevelWatcherClass.changeMusic = 1;
			}
		}
	}
}
